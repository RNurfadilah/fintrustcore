FinTrustCore Solution

Overview:
FinTrustCore is a comprehensive financial solution consisting of three key microservices: AccountService, TransactionService, and UserManagement. Each service is dedicated to specific functions in financial transactions and user management.

Prerequisites:

.NET 8.0 SDK
Docker (latest version recommended)
SQL Server (latest version for compatibility)
Git (for repository cloning)
An IDE supporting .NET (e.g., Visual Studio, VS Code)
Installation:

Step 1: Clone the Repository
First, ensure Git is installed on your system. Then, execute the following command to clone the repository:
git clone https://gitlab.com/RNurfadilah/fintrustcore.git

Alternatively, download the repository directly from GitLab if you prefer not using Git.

Step 2: Setting Up the Database
Install and run SQL Server. Create three separate databases for the microservices:

FinTrustCore_UserDb
FinTrustCore_AccountDb
FinTrustCore_TransactionDb
Use SQL Server Management Studio or command-line tools for database creation.

Step 3: Update Database Connection Strings
In the appsettings.json file of each microservice, update the connection strings to correspond with your SQL Server instance. The typical format is:
"Server=YourServer;Database=YourDB;User Id=YourUsername;Password=YourPassword;"

Step 4: Applying Migrations
Navigate to each microservice directory and apply migrations to establish the database schema. Example for one microservice:
cd AccountService
dotnet ef database update

Repeat for each microservice.

Step 5: Building and Running with Docker
Ensure Docker Desktop is operational. In the root directory with docker-compose.yml, run:
docker-compose up --build

This builds and starts all service containers.

Step 6: Verifying the Setup
Once running, access the services at:

AccountService: http://localhost:5001/
TransactionService: http://localhost:5002/
UserManagement: http://localhost:5000/
Use tools like Postman or Swagger UI for API interaction.

Troubleshooting:
For setup issues, ensure all configurations are correct. Contact [Your Contact Information] for further assistance.

Cleanup:
To stop and remove Docker containers post-testing:
docker-compose down

Version Updates:
Pull the latest changes from the repository as needed.