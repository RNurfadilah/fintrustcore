using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using AccountService.Data;
using AccountService.Dto;
using AccountService.Services;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;

var builder = WebApplication.CreateBuilder(args);

#region Database and Services Configuration
builder.Services.AddDbContext<AccountServiceDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("FinTrustCoreDB")));

builder.Services.AddScoped<AccountManagementService>();
builder.Services.AddScoped<AccountRepository>();
builder.Services.AddHttpClient();
#endregion

#region JWT Authentication Configuration
var jwtSettings = builder.Configuration.GetSection("JwtSettings");
builder.Services.AddAuthentication(opt =>
{
    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
.AddJwtBearer(options => options.TokenValidationParameters = GetTokenValidationParameters(jwtSettings));

builder.Services.AddAuthorization();
#endregion

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

#region HTTP Request Pipeline Configuration
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();
#endregion

#region Account Management Endpoints
app.MapPost("/api/accounts/create", async (HttpContext httpContext, AccountManagementService accountService, AccountCreationDto accountCreationDto) =>
    await CreateAccount(httpContext, accountService, accountCreationDto)).RequireAuthorization();

app.MapPost("/api/accounts/deposit", async (HttpContext httpContext, DepositDto depositDto, AccountRepository accountRepository, IHttpClientFactory httpClientFactory) =>
    await Deposit(httpContext, depositDto, accountRepository, httpClientFactory)).RequireAuthorization();

app.MapPost("/api/accounts/withdraw", async (HttpContext httpContext, WithdrawalDto withdrawalDto, AccountRepository accountRepository, IHttpClientFactory httpClientFactory) =>
    await Withdraw(httpContext, withdrawalDto, accountRepository, httpClientFactory)).RequireAuthorization();

app.MapPost("/api/accounts/transfer", async (HttpContext httpContext, TransferDto transferDto, AccountRepository accountRepository, IHttpClientFactory httpClientFactory) =>
    await TransferFunds(httpContext, transferDto, accountRepository, httpClientFactory)).RequireAuthorization();
#endregion

app.Run();

#region Helper Methods
TokenValidationParameters GetTokenValidationParameters(IConfigurationSection jwtSettings) => new TokenValidationParameters
{
    ValidateIssuer = true,
    ValidateAudience = true,
    ValidateLifetime = true,
    ValidateIssuerSigningKey = true,
    ValidIssuer = jwtSettings["Issuer"],
    ValidAudience = jwtSettings["Audience"],
    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings["SecretKey"]))
};

async Task<IResult> CreateAccount(HttpContext httpContext, AccountManagementService accountService, AccountCreationDto accountCreationDto)
{
    if (!httpContext.User.Identity.IsAuthenticated)
    {
        return Results.Unauthorized();
    }

    var userIdClaim = httpContext.User.Claims.FirstOrDefault(c => c.Type == "UserID");
    if (userIdClaim == null || !int.TryParse(userIdClaim.Value, out int userId))
    {
        return Results.Problem("Invalid user identification.", statusCode: 401);
    }

    var account = await accountService.CreateAccountAsync(userId, accountCreationDto.AccountType);
    return Results.Ok(account);
}

async Task<IResult> Deposit(HttpContext httpContext, DepositDto depositDto, AccountRepository accountRepository, IHttpClientFactory httpClientFactory)
{
    if (!httpContext.User.Identity.IsAuthenticated)
    {
        return Results.Unauthorized();
    }

    // Extract user ID from the token claims
    var userIdClaim = httpContext.User.Claims.FirstOrDefault(c => c.Type == "UserID");
    if (userIdClaim == null || !int.TryParse(userIdClaim.Value, out int userId))
    {
        return Results.Problem("Invalid user identification.", statusCode: 401);
    }

    // Find the account associated with the user ID
    var account = await accountRepository.GetAccountByUserIdAsync(userId);
    if (account == null)
    {
        return Results.NotFound("Account not found.");
    }

    // Perform the deposit operation
    account.Balance += depositDto.Amount;
    await accountRepository.UpdateAccountAsync(account);

    // Prepare and send the transaction record
    var transactionRecord = new
    {
        AccountId = account.AccountId,
        Type = "Deposit",
        Amount = depositDto.Amount,
        TransactionDate = DateTime.UtcNow,
        Status = "Completed",
        Remarks = "Deposit from personal wealth"
    };

    var recordResult = await RecordTransaction(httpContext, httpClientFactory, transactionRecord);

    return Results.Ok(new { Message = "Deposit successful", NewBalance = account.Balance });
}

async Task<IResult> Withdraw(HttpContext httpContext, WithdrawalDto withdrawalDto, AccountRepository accountRepository, IHttpClientFactory httpClientFactory)
{
    if (!httpContext.User.Identity.IsAuthenticated)
    {
        return Results.Unauthorized();
    }

    // Extract user ID from the token claims
    var userIdClaim = httpContext.User.Claims.FirstOrDefault(c => c.Type == "UserID");
    if (userIdClaim == null || !int.TryParse(userIdClaim.Value, out int userId))
    {
        return Results.Problem("Invalid user identification.", statusCode: 401);
    }

    // Find the account associated with the user ID
    var account = await accountRepository.GetAccountByUserIdAsync(userId);
    if (account == null)
    {
        return Results.NotFound("Account not found.");
    }

    // Check if the account has sufficient balance for the withdrawal
    if (account.Balance < withdrawalDto.Amount)
    {
        return Results.BadRequest("Insufficient funds for withdrawal.");
    }

    // Perform the withdrawal operation
    account.Balance -= withdrawalDto.Amount;
    await accountRepository.UpdateAccountAsync(account);

    // Prepare and send the transaction record
    var transactionRecord = new
    {
        AccountId = account.AccountId,
        Type = "Withdrawal",
        Amount = withdrawalDto.Amount,
        TransactionDate = DateTime.UtcNow,
        Status = "Completed",
        Remarks = "Withdrawal to personal wallet"
    };

    var recordResult = await RecordTransaction(httpContext, httpClientFactory, transactionRecord);

    return Results.Ok(new { Message = "Withdrawal successful", NewBalance = account.Balance });
}

async Task<IResult> TransferFunds(HttpContext httpContext, TransferDto transferDto, AccountRepository accountRepository, IHttpClientFactory httpClientFactory)
{
    if (!httpContext.User.Identity.IsAuthenticated)
    {
        return Results.Unauthorized();
    }

    // Extract user ID from the token claims
    var userIdClaim = httpContext.User.Claims.FirstOrDefault(c => c.Type == "UserID");
    if (userIdClaim == null || !int.TryParse(userIdClaim.Value, out int userId))
    {
        return Results.Problem("Invalid user identification.", statusCode: 401);
    }

    // Fetch the account associated with the user ID (assuming it's the 'from' account)
    var fromAccount = await accountRepository.GetAccountByUserIdAsync(userId);
    var toAccount = await accountRepository.GetAccountByIdAsync(transferDto.ToAccountId);

    if (fromAccount == null || toAccount == null)
    {
        return Results.NotFound("One or both accounts not found.");
    }

    if (fromAccount.Balance < transferDto.Amount)
    {
        return Results.BadRequest("Insufficient funds for transfer.");
    }

    // Transfer logic
    fromAccount.Balance -= transferDto.Amount;
    toAccount.Balance += transferDto.Amount;

    await accountRepository.UpdateAccountAsync(fromAccount);
    await accountRepository.UpdateAccountAsync(toAccount);

    // Prepare and send the transaction record
    var transactionRecord = new
    {
        AccountId = fromAccount.AccountId,
        Type = "Transfer",
        Amount = transferDto.Amount,
        TransactionDate = DateTime.UtcNow,
        Status = "Completed",
        Remarks = $"Transfer to account {toAccount.AccountId}"
    };

    var recordResult = await RecordTransaction(httpContext, httpClientFactory, transactionRecord);
    if (!recordResult.IsSuccess)
    {
        // Rollback the changes made to account balances if transaction recording fails
        fromAccount.Balance += transferDto.Amount;
        toAccount.Balance -= transferDto.Amount;
        await accountRepository.UpdateAccountAsync(fromAccount);
        await accountRepository.UpdateAccountAsync(toAccount);

        Console.WriteLine($"Failed to record the transaction in the transaction service. Changes rolled back. Response: {recordResult.ResponseContent}");
        return Results.Problem($"Failed to record the transaction. Response: {recordResult.ResponseContent}", statusCode: 500);
    }

    return Results.Ok(new { Message = "Transfer successful" });
}

async Task<(bool IsSuccess, string ResponseContent)> RecordTransaction(HttpContext httpContext, IHttpClientFactory httpClientFactory, object transactionRecord)
{
    var httpClient = httpClientFactory.CreateClient();
    var request = new HttpRequestMessage(HttpMethod.Post, "http://transactionservice:8080/api/transactions/record")
    {
        Content = new StringContent(JsonConvert.SerializeObject(transactionRecord), Encoding.UTF8, "application/json")
    };

    var incomingToken = httpContext.Request.Headers["Authorization"].FirstOrDefault();
    if (!string.IsNullOrEmpty(incomingToken))
    {
        request.Headers.TryAddWithoutValidation("Authorization", incomingToken);
    }

    var response = await httpClient.SendAsync(request);
    var responseContent = await response.Content.ReadAsStringAsync();

    return (response.IsSuccessStatusCode, responseContent);
}
#endregion
