using AccountService.Data;
using AccountService.Models;

namespace AccountService.Services
{
    public class AccountManagementService
    {
        private readonly AccountRepository _accountRepository;

        public AccountManagementService(AccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        public async Task<Account> CreateAccountAsync(int userId, string accountType)
        {
            var random = new Random();
            var accountNumber = GenerateAccountNumber(random);

            // Keep generating a new number until a unique one is found
            while (await _accountRepository.AccountNumberExistsAsync(accountNumber))
            {
                accountNumber = GenerateAccountNumber(random);
            }

            var account = new Account
            {
                UserId = userId,
                AccountNumber = accountNumber,
                AccountType = accountType,
                Balance = 1000000, // Set default balance
                CreatedAt = DateTime.UtcNow
            };

            return await _accountRepository.CreateAccountAsync(account);
        }

        private string GenerateAccountNumber(Random random)
        {
            return random.Next(10000000, 99999999).ToString();
        }
    }
}