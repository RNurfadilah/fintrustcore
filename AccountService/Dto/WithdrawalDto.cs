namespace AccountService.Dto
{
    public class WithdrawalDto
    {
        public decimal Amount { get; set; }
    }
}