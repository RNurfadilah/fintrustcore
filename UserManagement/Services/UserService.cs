using System.Text;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using UserManagement.Data;
using UserManagement.Dtos;
using UserManagement.Models;
using UserManagement.Utilities;

namespace UserManagement.Services
{
    public class UserService
    {
        private readonly UserManagementDbContext _context;
        private readonly UserRepository _userRepository;
        private readonly PasswordHasher _passwordHasher;

        public UserService(UserManagementDbContext context, UserRepository userRepository, PasswordHasher passwordHasher)
        {
            _context = context;
            _userRepository = userRepository;
            _passwordHasher = passwordHasher;
        }

        public async Task RegisterUserAsync(User user)
        {
            // Save the user and get the generated UserId
            await _userRepository.AddUserAsync(user);
        }

        public async Task<User> AuthenticateAsync(string username, string password)
        {
            var user = await _userRepository.FindByUsernameAsync(username);
            if (user != null && _passwordHasher.VerifyPassword(user.PasswordHash, password))
            {
                return user;
            }
            return null;
        }

        public async Task<User> GetUserByUsernameAsync(string username)
        {
            return await _context.Users
                                 .FirstOrDefaultAsync(u => u.Username == username);
        }

        public async Task<bool> UpdateUserProfileAsync(int userId, UpdateUserProfileDto updateDto)
        {
            var user = await _context.Users.FindAsync(userId);
            if (user == null)
            {
                return false;
            }

            // Update user's properties
            user.FullName = updateDto.FullName;
            user.Email = updateDto.Email;

            _context.Users.Update(user);
            int saveResult = await _context.SaveChangesAsync();
            return saveResult > 0;
        }

        public async Task<User> GetUserByIdAsync(int userId)
        {
            return await _context.Users.FindAsync(userId);
        }
    }
}
