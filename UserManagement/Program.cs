using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using UserManagement.Data;
using UserManagement.Dtos;
using UserManagement.Models;
using UserManagement.Services;
using UserManagement.Utilities;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;

var builder = WebApplication.CreateBuilder(args);

#region Database and Services Configuration
builder.Services.AddDbContext<UserManagementDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("FinTrustCoreDB")));

builder.Services.AddScoped<UserService>();
builder.Services.AddScoped<UserRepository>();
builder.Services.AddSingleton<PasswordHasher>();
#endregion

#region JWT Authentication Configuration
var jwtSettings = builder.Configuration.GetSection("JwtSettings");
builder.Services.AddAuthentication(opt =>
{
    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
.AddJwtBearer(options => options.TokenValidationParameters = GetTokenValidationParameters(jwtSettings));

builder.Services.AddAuthorization();
#endregion

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();

app.MapPost("/api/users/register", async (UserService userService, PasswordHasher passwordHasher, RegisterUserDto registerUserDto) => await RegisterUser(userService, passwordHasher, registerUserDto));
app.MapPost("/api/users/login", async (UserService userService, LoginDto loginDto) => await LoginUser(userService, loginDto, jwtSettings));
app.MapGet("/api/users/profile", async (HttpContext httpContext, UserService userService) => await GetUserProfile(httpContext, userService)).RequireAuthorization();
app.MapPut("/api/users/profile", async (HttpContext httpContext, UserService userService, UpdateUserProfileDto updateDto) => await UpdateUserProfile(httpContext, userService, updateDto)).RequireAuthorization();

app.Run();

#region Helper Methods
TokenValidationParameters GetTokenValidationParameters(IConfigurationSection jwtSettings) => new TokenValidationParameters
{
    ValidateIssuer = true,
    ValidateAudience = true,
    ValidateLifetime = true,
    ValidateIssuerSigningKey = true,
    ValidIssuer = jwtSettings["Issuer"],
    ValidAudience = jwtSettings["Audience"],
    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings["SecretKey"])),
    NameClaimType = JwtRegisteredClaimNames.Sub
};

string GenerateJwtToken(User user, IConfigurationSection jwtSettings)
{
    var key = Encoding.UTF8.GetBytes(jwtSettings["SecretKey"]);
    var securityKey = new SymmetricSecurityKey(key);
    var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

    var claims = new List<Claim>
    {
        new Claim("UserID", user.UserId.ToString()),
        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
        new Claim(JwtRegisteredClaimNames.Email, user.Email)
    };

    var token = new JwtSecurityToken(
        issuer: jwtSettings["Issuer"],
        audience: jwtSettings["Audience"],
        claims: claims,
        expires: DateTime.UtcNow.AddHours(1),
        signingCredentials: credentials
    );

    return new JwtSecurityTokenHandler().WriteToken(token);
}

async Task<IResult> RegisterUser(UserService userService, PasswordHasher passwordHasher, RegisterUserDto registerUserDto)
{
    try
    {
        var user = new User
        {
            Username = registerUserDto.Username,
            PasswordHash = passwordHasher.HashPassword(registerUserDto.Password),
            FullName = registerUserDto.FullName,
            Email = registerUserDto.Email,
            DateOfBirth = registerUserDto.DateOfBirth,
            CreatedAt = DateTime.UtcNow,
            UpdatedAt = null
        };

        await userService.RegisterUserAsync(user);
        return Results.Ok("User registered successfully");
    }
    catch (Exception ex)
    {
        return Results.Problem(ex.Message);
    }
}

async Task<IResult> LoginUser(UserService userService, LoginDto loginDto, IConfigurationSection jwtSettings)
{
    var user = await userService.AuthenticateAsync(loginDto.Username, loginDto.Password);
    if (user != null)
    {
        var token = GenerateJwtToken(user, jwtSettings);
        return Results.Ok(new { token });
    }
    return Results.Unauthorized();
}

async Task<IResult> GetUserProfile(HttpContext httpContext, UserService userService)
{
    if (!httpContext.User.Identity.IsAuthenticated)
    {
        return Results.Unauthorized();
    }

    var userIdClaim = httpContext.User.Claims.FirstOrDefault(c => c.Type == "UserID");
    if (userIdClaim == null)
    {
        return Results.Problem("UserID claim not found in token.", statusCode: 401);
    }

    if (!int.TryParse(userIdClaim.Value, out int userId))
    {
        return Results.Problem("Invalid UserID claim.", statusCode: 401);
    }

    var user = await userService.GetUserByIdAsync(userId);
    if (user == null)
    {
        return Results.NotFound("User not found.");
    }

    var userProfile = new UserProfileDto
    {
        Username = user.Username,
        FullName = user.FullName,
        Email = user.Email,
        DateOfBirth = user.DateOfBirth
    };

    return Results.Ok(userProfile);
}

async Task<IResult> UpdateUserProfile(HttpContext httpContext, UserService userService, UpdateUserProfileDto updateDto)
{
    if (!httpContext.User.Identity.IsAuthenticated)
    {
        return Results.Unauthorized();
    }

    var userIdClaim = httpContext.User.Claims.FirstOrDefault(c => c.Type == "UserID");
    if (userIdClaim == null)
    {
        return Results.Problem("UserID claim not found in token.", statusCode: 401);
    }

    if (!int.TryParse(userIdClaim.Value, out int userId))
    {
        return Results.Problem("Invalid UserID claim.", statusCode: 401);
    }

    var result = await userService.UpdateUserProfileAsync(userId, updateDto);
    if (result)
    {
        return Results.Ok("Profile updated successfully.");
    }

    return Results.BadRequest("Failed to update profile.");
}
#endregion

public record LoginDto(string Username, string Password);
