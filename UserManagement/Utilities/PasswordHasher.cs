using Microsoft.AspNetCore.Identity;

namespace UserManagement.Utilities
{
    public class PasswordHasher
    {
        public string HashPassword(string password)
        {
            var hasher = new PasswordHasher<object>();
            return hasher.HashPassword(new object(), password); // Pass a new object instance
        }

        public bool VerifyPassword(string hashedPassword, string password)
        {
            var hasher = new Microsoft.AspNetCore.Identity.PasswordHasher<object>();
            return hasher.VerifyHashedPassword(null!, hashedPassword, password) == PasswordVerificationResult.Success;
        }
    }
}