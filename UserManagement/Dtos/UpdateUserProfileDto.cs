namespace UserManagement.Dtos
{
    public record UpdateUserProfileDto
    {
        public string FullName { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
    }
}