using Microsoft.EntityFrameworkCore;
using TransactionService.Models;

namespace TransactionService.Data
{
    public class TransactionServiceDbContext : DbContext
    {
        public TransactionServiceDbContext(DbContextOptions<TransactionServiceDbContext> options)
            : base(options)
        {
        }

        public DbSet<BankTransaction> BankTransactions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Specify the precision and scale for the Amount property in BankTransaction
            modelBuilder.Entity<BankTransaction>()
                .Property(t => t.Amount)
                .HasPrecision(18, 2);
        }
    }
}