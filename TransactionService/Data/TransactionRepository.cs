using Microsoft.EntityFrameworkCore;
using TransactionService.Models;

namespace TransactionService.Data
{
    public class TransactionRepository
    {
        private readonly TransactionServiceDbContext _context;

        public TransactionRepository(TransactionServiceDbContext context)
        {
            _context = context;
        }

        public async Task<BankTransaction> AddTransactionAsync(BankTransaction transaction)
        {
            _context.BankTransactions.Add(transaction);
            await _context.SaveChangesAsync();
            return transaction;
        }

        public async Task<IEnumerable<BankTransaction>> GetTransactionsByUserIdAsync(int userId, DateTime? startDate, DateTime? endDate)
        {
            var query = _context.BankTransactions.Where(t => t.AccountId == userId);

            if (startDate.HasValue)
            {
                var startOfDay = startDate.Value.Date; // Start of the day
                query = query.Where(t => t.TransactionDate >= startOfDay);
            }

            if (endDate.HasValue)
            {
                var endOfDay = endDate.Value.Date.AddDays(1).AddTicks(-1); // End of the day
                query = query.Where(t => t.TransactionDate <= endOfDay);
            }

            return await query.ToListAsync();
        }
    }
}