namespace TransactionService.Models
{
    public class BankTransaction
    {
        public int BankTransactionId { get; set; }
        public int AccountId { get; set; } // Foreign key to Account
        public string Type { get; set; } = string.Empty;
        public decimal Amount { get; set; }
        public DateTime TransactionDate { get; set; }
        public string Status { get; set; } = string.Empty;
        public string Remarks { get; set; } = string.Empty;
    }
}
