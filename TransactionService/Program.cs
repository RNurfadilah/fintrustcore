using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using TransactionService.Data;
using TransactionService.Models;
using Microsoft.Extensions.Logging;

var builder = WebApplication.CreateBuilder(args);

#region Database and Services Configuration
builder.Services.AddDbContext<TransactionServiceDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("FinTrustCoreDB")));

builder.Services.AddScoped<TransactionRepository>();
#endregion

#region JWT Authentication Configuration
var jwtSettings = builder.Configuration.GetSection("JwtSettings");
builder.Services.AddAuthentication(opt =>
{
    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
.AddJwtBearer(options => options.TokenValidationParameters = GetTokenValidationParameters(jwtSettings));

builder.Services.AddAuthorization();
#endregion

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddLogging();

var app = builder.Build();

#region HTTP Request Pipeline Configuration
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();
#endregion

#region Transaction Endpoints
app.MapPost("/api/transactions/record", async (HttpContext httpContext, BankTransaction bankTransaction, TransactionRepository transactionRepository, ILogger<Program> logger) =>
    await RecordTransaction(httpContext, bankTransaction, transactionRepository, logger)).RequireAuthorization();

app.MapGet("/api/transactions/details", async (HttpContext httpContext, DateTime? startDate, DateTime? endDate, TransactionRepository transactionRepository, ILogger<Program> logger) =>
    await GetTransactionDetails(httpContext, startDate, endDate, transactionRepository, logger)).RequireAuthorization();
#endregion

app.Run();

#region Helper Methods
TokenValidationParameters GetTokenValidationParameters(IConfigurationSection jwtSettings) => new TokenValidationParameters
{
    ValidateIssuer = true,
    ValidateAudience = true,
    ValidateLifetime = true,
    ValidateIssuerSigningKey = true,
    ValidIssuer = jwtSettings["Issuer"],
    ValidAudience = jwtSettings["Audience"],
    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings["SecretKey"]))
};

async Task<IResult> RecordTransaction(HttpContext httpContext, BankTransaction bankTransaction, TransactionRepository transactionRepository, ILogger<Program> logger)
{
    logger.LogInformation("Received request to record transaction.");

    if (!httpContext.User.Identity.IsAuthenticated)
    {
        logger.LogWarning("Unauthorized access attempt. Token: {Token}", httpContext.Request.Headers["Authorization"].FirstOrDefault());
        return Results.Unauthorized();
    }

    try
    {
        logger.LogInformation("Attempting to add transaction to database.");
        await transactionRepository.AddTransactionAsync(bankTransaction);
        logger.LogInformation("Transaction recorded successfully.");
        return Results.Ok("Transaction recorded successfully");
    }
    catch (Exception ex)
    {
        logger.LogError(ex, "Error occurred while recording the transaction.");
        return Results.Problem("An error occurred while processing the transaction.");
    }
}

async Task<IResult> GetTransactionDetails(HttpContext httpContext, DateTime? startDate, DateTime? endDate, TransactionRepository transactionRepository, ILogger<Program> logger)
{
    logger.LogInformation("Received request to get transaction details.");

    if (!httpContext.User.Identity.IsAuthenticated)
    {
        logger.LogWarning("Unauthorized access attempt to transaction details endpoint.");
        return Results.Unauthorized();
    }

    var userIdClaim = httpContext.User.Claims.FirstOrDefault(c => c.Type == "UserID");
    if (userIdClaim == null || !int.TryParse(userIdClaim.Value, out int userId))
    {
        return Results.Problem("Invalid user identification.", statusCode: 401);
    }

    try
    {
        var transactions = await transactionRepository.GetTransactionsByUserIdAsync(userId, startDate, endDate);
        return transactions.Any() ? Results.Ok(transactions) : Results.NotFound("No transactions found.");
    }
    catch (Exception ex)
    {
        logger.LogError(ex, "Error occurred while fetching transaction details.");
        return Results.Problem("An error occurred while processing the transaction details.");
    }
}
#endregion
